var colorPalette = (function () {

    var fromIdToCssDict = {
        'fallBricksColor': '.tile.on',
        'activeBricksColor' : '.tile.now'
    };

    // Нарисовать игровое поле.
    var createColorPickers = function (containerId) {
        let container = document.getElementById(containerId);
        let activeBrickElement = _createColorPicker("activeBricksColor", "colorPicker", "#008000");
        let fallBricksColor = _createColorPicker("fallBricksColor", "colorPicker", "#000000");
        container.appendChild(activeBrickElement);
        container.appendChild(fallBricksColor);

        document.getElementById("activeBricksColor").addEventListener("change", changeTileColor, false);
        document.getElementById("fallBricksColor").addEventListener("change", changeTileColor, false);
        
    };
    
    var _createColorPicker = function (id, className, defaultValue) {
        let picker = document.createElement('input');
        picker.setAttribute("id", id);
        picker.setAttribute("type", "color");
        picker.setAttribute("value", defaultValue);
        picker.classList.add(className);
        return picker;
    }

    var _changeColor = function(color, styleName) {
        let styleSheets = document.styleSheets[0];
        for (let i = 0; i < styleSheets.cssRules.length; i++) {
            let rule = styleSheets.cssRules[i];
            if (rule.selectorText == styleName) {
                styleSheets.deleteRule(i);
                styleSheets.insertRule(styleName + "{ background-color:" + color + "; }", 0);
            }
        }
    }

    var changeTileColor = function(event) {
        if (event) _changeColor(event.target.value, fromIdToCssDict[event.target.id]);
    }

    return {
        changeTileColor : changeTileColor,
        createColorPickers: createColorPickers
    }
})();