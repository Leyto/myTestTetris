var tetris = (function () {
   // Набор игровых фигур.
    var figures = "1111:01|01|01|01*011|110:010|011|001*110|011:001|011|010*111|010:01|11|01:010|111:10|11|10*11|11*010|010|011:111|100:11|01|01:001|111*01|01|11:100|111:11|10|10:111|001", now = [3,0], pos = [4,0];
    var isPause = false;
    var gameVelocity = 0.3;

    var _createPlayground = function (containerId) {
        let container = document.getElementById(containerId);
        let playground = document.createElement('div');
        playground.setAttribute("id", "stack");
        for (let i = 0; i < 20; i++) {
            let line = document.createElement('div');
            line.setAttribute("data-y", i);
            for (let j = 0; j < 10; j++) {
                var tile = document.createElement('div');
                tile.setAttribute("data-x", j);
                tile.classList.add("tile");
                line.appendChild(tile);
                playground.appendChild(line);
            }
        }
        let pause = document.createElement('div');
        pause.classList.add("pauseAttention");
        pause.setAttribute("id", "pauseAttention");
        playground.appendChild(pause);
        container.appendChild(playground)
    };

    var _getTile = function(x, y) {
        return document.querySelector('[data-y="' + y + '"] [data-x="' + x + '"]');
    };

    function setPause()
    {
        if (delay > 0) {
            isPause = !isPause;
            delay = -delay;
            document.getElementById("pauseAttention").innerHTML = "ПАУЗА";
        }
        else {
            isPause = !isPause;
            delay = -delay;
            document.getElementById("pauseAttention").innerHTML = "";
            _startGame(delay);
        }
    }
    
    var _draw = function(ch, cls) {
        var f = figures.split('*')[now[0]].split(':')[now[1]].split('|').map(function(a) {return a.split('')});
        for (let y = 0; y < f.length; y++) {
            for (let x = 0; x < f[y].length; x++) {
                if (f[y][x] == '1') {
                    if (x + pos[0] + ch[0] > 9
                        || x + pos[0] + ch[0] < 0
                        || y + pos[1] + ch[1] > 19
                        || _getTile(x + pos[0] + ch[0], y + pos[1] + ch[1]).classList.contains('on'))
                    return false;

                    _getTile(x + pos[0] + ch[0], y + pos[1] + ch[1]).classList.add(cls !== undefined ? cls : 'now');
                }
            }
        }
        pos = [pos[0] + ch[0], pos[1] + ch[1]];
    }

    var _deDraw = function() {
        if (document.querySelectorAll('.now').length > 0) {
            _deDraw(document.querySelector('.now').classList.remove('now'));
        }
    }

    var _check = function() {
        for (let i = 0; i < 20; i++)
        {
            if (document.querySelectorAll('[data-y="' + i + '"] .tile.on').length == 10) {
                return _check(_roll(i), document.querySelector('#result').innerHTML = Math.floor(document.querySelector('#result').innerHTML)+10);
            }
        }
    };

    var _roll = function(ln) {
        if (false !== (document.querySelector('[data-y="' + ln + '"]').innerHTML = document.querySelector('[data-y="' + (ln-1) + '"]').innerHTML) && ln > 1)
        _roll(ln-1);
    };

    window.addEventListener('keydown', kdf = function(e) {
        if (!isPause)
        {
            if (e.keyCode == 38 && false !== (now[1]=((prv=now[1])+1)%figures.split('*')[now[0]].split(':').length) && false === _draw([0,0], undefined, _deDraw()))
            _draw([0,0], undefined, _deDraw(), now=[now[0],prv]);
            if ((e.keyCode == 39 || e.keyCode == 37) && false === _draw([e.keyCode == 39 ? 1 :-1,0], undefined, _deDraw()))
            _draw([0,0], undefined, _deDraw());
            if (e.keyCode == 40 && false === _draw([0,1], undefined, _deDraw())) {
                if (_draw([0,0], 'on', _deDraw()) || true) _check();
                if (false === _draw([0,0], undefined, now = [Math.floor(Math.random() * figures.split('*').length), 0], pos = [4,0])) { 
                    delay=-1; 
                    alert('Поздравляем! Ваш счёт: ' + document.querySelector('#result').innerHTML); 
                }
            }
        }
    });

    var _startGame = function() {
        kdf({keyCode: 40});
        setTimeout(function() {
            if(delay >= 0) _startGame();
            console.log("!!!");
            },
            delay = delay > 0 ? delay - gameVelocity : delay
        );
    }

    function newGame(containerId) {
        if (document.getElementById("stack") !== null)
            document.getElementById("stack").remove();
        if (document.querySelector('#result') !== null)
            document.querySelector('#result').innerHTML = 0;
            _createPlayground(containerId);
            _startGame(delay = 500);
        setPause();
    }

    return {
        setPause : setPause,
        newGame : newGame,
    }
})();